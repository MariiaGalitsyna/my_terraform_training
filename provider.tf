terraform {
  backend "s3" {
    bucket = "mg-tfm-bkt"
    key = "terraform.tfstate"
    region = "eu-central-1"
    encrypt = true
  }
}

provider "aws" {
    
}
